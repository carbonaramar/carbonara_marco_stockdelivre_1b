-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 30 Avril 2020 à 12:17
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `carbonara_marco_stock_livre_bd_104_2020`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_books_a_auteur`
--

CREATE TABLE `t_books_a_auteur` (
  `id_Books_A_Auteur` int(11) NOT NULL,
  `FK_Auteur` int(11) NOT NULL,
  `FK_Books` int(11) NOT NULL,
  `Date_Books_A_Auteur` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_books_a_auteur`
--

INSERT INTO `t_books_a_auteur` (`id_Books_A_Auteur`, `FK_Auteur`, `FK_Books`, `Date_Books_A_Auteur`) VALUES
(1, 2, 3, '2020-04-29'),
(2, 5, 4, '2020-04-22'),
(3, 6, 5, '2020-04-29'),
(4, 3, 6, '2020-04-29'),
(5, 4, 2, '2020-04-29');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_books_a_auteur`
--
ALTER TABLE `t_books_a_auteur`
  ADD PRIMARY KEY (`id_Books_A_Auteur`),
  ADD KEY `FK_Auteur` (`FK_Auteur`),
  ADD KEY `FK_Books` (`FK_Books`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_books_a_auteur`
--
ALTER TABLE `t_books_a_auteur`
  MODIFY `id_Books_A_Auteur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_books_a_auteur`
--
ALTER TABLE `t_books_a_auteur`
  ADD CONSTRAINT `t_books_a_auteur_ibfk_1` FOREIGN KEY (`FK_Books`) REFERENCES `t_books` (`id_Books`),
  ADD CONSTRAINT `t_books_a_auteur_ibfk_2` FOREIGN KEY (`FK_Auteur`) REFERENCES `t_auteur` (`id_Auteur`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
