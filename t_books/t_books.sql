-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 30 Avril 2020 à 12:17
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `carbonara_marco_stock_livre_bd_104_2020`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_books`
--

CREATE TABLE `t_books` (
  `id_Books` int(11) NOT NULL,
  `Isbn_Books` int(11) NOT NULL,
  `Titre_Books` varchar(22) NOT NULL,
  `Country_ Books` varchar(12) NOT NULL,
  `Date_Books` date NOT NULL,
  `Publieur_Books` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_books`
--

INSERT INTO `t_books` (`id_Books`, `Isbn_Books`, `Titre_Books`, `Country_ Books`, `Date_Books`, `Publieur_Books`) VALUES
(2, 1004, 'Skurrt', 'Swiss', '2020-03-03', 'Baptispe'),
(3, 200002, 'The Call of Cthulhu', 'UnitedStates', '2020-04-06', 'the pulp magazine Weird Tales'),
(4, 1869, 'Ma vie', 'Belgique', '2013-03-28', 'LE CHERCHE MIDI'),
(5, 1234, 'Folow Me', 'France', '2019-10-16', 'Glénat'),
(6, 1003, 'LesAnimauxfantastiques', 'royaume uni', '2001-03-12', 'Bloomsbury'),
(7, 667, 'FreezeCorleone:Périple', 'Ukraine', '2020-04-30', '667');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_books`
--
ALTER TABLE `t_books`
  ADD PRIMARY KEY (`id_Books`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_books`
--
ALTER TABLE `t_books`
  MODIFY `id_Books` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
