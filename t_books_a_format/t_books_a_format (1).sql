-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 30 Avril 2020 à 12:18
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `carbonara_marco_stock_livre_bd_104_2020`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_books_a_format`
--

CREATE TABLE `t_books_a_format` (
  `id_Books_A_Format` int(11) NOT NULL,
  `FK_Books` int(11) NOT NULL,
  `FK_Format` int(11) NOT NULL,
  `Date_Books_A_Format` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_books_a_format`
--

INSERT INTO `t_books_a_format` (`id_Books_A_Format`, `FK_Books`, `FK_Format`, `Date_Books_A_Format`) VALUES
(1, 2, 1, '2020-04-29'),
(2, 6, 2, '2020-04-29'),
(3, 4, 3, '2020-04-29'),
(4, 5, 4, '2020-04-29'),
(5, 3, 5, '2020-04-29'),
(6, 2, 6, '2020-04-29');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_books_a_format`
--
ALTER TABLE `t_books_a_format`
  ADD PRIMARY KEY (`id_Books_A_Format`),
  ADD KEY `FK_Books` (`FK_Books`),
  ADD KEY `FK_Format` (`FK_Format`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_books_a_format`
--
ALTER TABLE `t_books_a_format`
  MODIFY `id_Books_A_Format` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_books_a_format`
--
ALTER TABLE `t_books_a_format`
  ADD CONSTRAINT `t_books_a_format_ibfk_1` FOREIGN KEY (`FK_Books`) REFERENCES `t_books` (`id_Books`),
  ADD CONSTRAINT `t_books_a_format_ibfk_2` FOREIGN KEY (`FK_Format`) REFERENCES `t_format` (`id_Format`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
