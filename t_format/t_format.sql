-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 30 Avril 2020 à 12:19
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `carbonara_marco_stock_livre_bd_104_2020`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_format`
--

CREATE TABLE `t_format` (
  `id_Format` int(11) NOT NULL,
  `Nom_Format` varchar(11) NOT NULL,
  `Edition_Format` varchar(22) NOT NULL,
  `Date_Format` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_format`
--

INSERT INTO `t_format` (`id_Format`, `Nom_Format`, `Edition_Format`, `Date_Format`) VALUES
(1, 'A7', 'KobalaD', '2020-03-09'),
(2, 'Roman', 'Bloomsbury', '2020-04-29'),
(3, 'BIOGRAPHIES', 'LE CHERCHE MIDI', '2020-04-29'),
(4, 'BD', 'Glénat', '2020-04-29'),
(5, 'Roman', 'pulpmagazineWeirdTales', '2020-04-29'),
(6, 'Brochure', 'Baptispe', '2020-04-29');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_format`
--
ALTER TABLE `t_format`
  ADD PRIMARY KEY (`id_Format`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_format`
--
ALTER TABLE `t_format`
  MODIFY `id_Format` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
