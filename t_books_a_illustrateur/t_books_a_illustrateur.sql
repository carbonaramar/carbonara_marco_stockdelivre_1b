-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 30 Avril 2020 à 12:18
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `carbonara_marco_stock_livre_bd_104_2020`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_books_a_illustrateur`
--

CREATE TABLE `t_books_a_illustrateur` (
  `id_Books_A_Illustrateur` int(11) NOT NULL,
  `FK_Books` int(11) NOT NULL,
  `FK_Illustrateur` int(11) NOT NULL,
  `Date_Books_A_Illustrateur` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_books_a_illustrateur`
--

INSERT INTO `t_books_a_illustrateur` (`id_Books_A_Illustrateur`, `FK_Books`, `FK_Illustrateur`, `Date_Books_A_Illustrateur`) VALUES
(1, 2, 5, '2020-04-29'),
(2, 3, 4, '2020-04-29'),
(3, 4, 3, '2020-04-29'),
(4, 5, 2, '2020-04-29'),
(5, 6, 1, '2020-04-29');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_books_a_illustrateur`
--
ALTER TABLE `t_books_a_illustrateur`
  ADD PRIMARY KEY (`id_Books_A_Illustrateur`),
  ADD KEY `FK_Books` (`FK_Books`),
  ADD KEY `FK_Illustrateur` (`FK_Illustrateur`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_books_a_illustrateur`
--
ALTER TABLE `t_books_a_illustrateur`
  MODIFY `id_Books_A_Illustrateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `t_books_a_illustrateur`
--
ALTER TABLE `t_books_a_illustrateur`
  ADD CONSTRAINT `t_books_a_illustrateur_ibfk_1` FOREIGN KEY (`FK_Books`) REFERENCES `t_books` (`id_Books`),
  ADD CONSTRAINT `t_books_a_illustrateur_ibfk_2` FOREIGN KEY (`FK_Illustrateur`) REFERENCES `t_illustrateur` (`id_Illustrateur`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
