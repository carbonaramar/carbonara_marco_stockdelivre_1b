-- phpMyAdmin SQL Dump
-- version 4.5.4.1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 30 Avril 2020 à 12:19
-- Version du serveur :  5.7.11
-- Version de PHP :  5.6.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `carbonara_marco_stock_livre_bd_104_2020`
--

-- --------------------------------------------------------

--
-- Structure de la table `t_illustrateur`
--

CREATE TABLE `t_illustrateur` (
  `id_Illustrateur` int(11) NOT NULL,
  `Nom_Illustrateur` varchar(11) NOT NULL,
  `Date_Illustrateur` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `t_illustrateur`
--

INSERT INTO `t_illustrateur` (`id_Illustrateur`, `Nom_Illustrateur`, `Date_Illustrateur`) VALUES
(1, 'Baka', '2020-03-10'),
(2, 'Didier', '2020-04-29'),
(3, 'PascalLeGFr', '2020-04-29'),
(4, 'MahdiBa', '2020-04-29'),
(5, 'Kamet0', '2020-04-29');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `t_illustrateur`
--
ALTER TABLE `t_illustrateur`
  ADD PRIMARY KEY (`id_Illustrateur`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `t_illustrateur`
--
ALTER TABLE `t_illustrateur`
  MODIFY `id_Illustrateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
